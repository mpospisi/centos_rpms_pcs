From 618dbcf1f7be271f63f399befdde93ced6448a52 Mon Sep 17 00:00:00 2001
From: Tomas Jelinek <tojeline@redhat.com>
Date: Wed, 12 Feb 2025 14:00:26 +0100
Subject: [PATCH 1/2] fix restarting bundle instances

* fixes a regression introduced in ccffba735128ea4be62aa33e7319114d8b26a8b0
---
 pcs/lib/commands/resource.py                  | 85 ++++++++++---------
 .../lib/commands/resource/test_restart.py     | 16 +---
 2 files changed, 46 insertions(+), 55 deletions(-)

diff --git a/pcs/lib/commands/resource.py b/pcs/lib/commands/resource.py
index 719f8acd3..945e40759 100644
--- a/pcs/lib/commands/resource.py
+++ b/pcs/lib/commands/resource.py
@@ -2566,56 +2566,59 @@ def restart(
     timeout -- abort if the command doesn't finish in this time (integer + unit)
     """
     cib = env.get_cib()
+
+    # To be able to restart bundle instances, which are not to be found in CIB,
+    # do not fail if specified ID is not found in CIB. Pacemaker provides
+    # reasonable messages when the ID to be restarted is not a resource or
+    # doesn't exist. We only search for the resource in order to provide hints
+    # when the user attempts to restart bundle's or clone's inner resources.
+    resource_found = False
     try:
         resource_el = get_element_by_id(cib, resource_id)
-    except ElementNotFound as e:
-        env.report_processor.report(
-            ReportItem.error(
-                reports.messages.IdNotFound(
-                    resource_id, expected_types=["resource"]
-                )
-            )
-        )
-        raise LibraryError() from e
-    if not resource.common.is_resource(resource_el):
-        env.report_processor.report(
-            ReportItem.error(
-                reports.messages.IdBelongsToUnexpectedType(
-                    resource_id,
-                    expected_types=["resource"],
-                    current_type=resource_el.tag,
+        resource_found = True
+    except ElementNotFound:
+        pass
+
+    if resource_found:
+        if not resource.common.is_resource(resource_el):
+            env.report_processor.report(
+                ReportItem.error(
+                    reports.messages.IdBelongsToUnexpectedType(
+                        resource_id,
+                        expected_types=["resource"],
+                        current_type=resource_el.tag,
+                    )
                 )
             )
-        )
-        raise LibraryError()
+            raise LibraryError()
 
-    parent_resource_el = resource.clone.get_parent_any_clone(resource_el)
-    if parent_resource_el is None:
-        parent_resource_el = resource.bundle.get_parent_bundle(resource_el)
-    if parent_resource_el is not None:
-        env.report_processor.report(
-            reports.ReportItem.warning(
-                reports.messages.ResourceRestartUsingParentRersource(
-                    str(resource_el.attrib["id"]),
-                    str(parent_resource_el.attrib["id"]),
+        parent_resource_el = resource.clone.get_parent_any_clone(resource_el)
+        if parent_resource_el is None:
+            parent_resource_el = resource.bundle.get_parent_bundle(resource_el)
+        if parent_resource_el is not None:
+            env.report_processor.report(
+                reports.ReportItem.warning(
+                    reports.messages.ResourceRestartUsingParentRersource(
+                        str(resource_el.attrib["id"]),
+                        str(parent_resource_el.attrib["id"]),
+                    )
                 )
             )
-        )
-        resource_el = parent_resource_el
+            resource_el = parent_resource_el
 
-    if node and not (
-        resource.clone.is_any_clone(resource_el)
-        or resource.bundle.is_bundle(resource_el)
-    ):
-        env.report_processor.report(
-            reports.ReportItem.error(
-                reports.messages.ResourceRestartNodeIsForMultiinstanceOnly(
-                    str(resource_el.attrib["id"]),
-                    resource_el.tag,
-                    node,
+        if node and not (
+            resource.clone.is_any_clone(resource_el)
+            or resource.bundle.is_bundle(resource_el)
+        ):
+            env.report_processor.report(
+                reports.ReportItem.error(
+                    reports.messages.ResourceRestartNodeIsForMultiinstanceOnly(
+                        str(resource_el.attrib["id"]),
+                        resource_el.tag,
+                        node,
+                    )
                 )
             )
-        )
 
     if timeout is not None:
         env.report_processor.report_list(
@@ -2627,7 +2630,7 @@ def restart(
 
     resource_restart(
         env.cmd_runner(),
-        str(resource_el.attrib["id"]),
+        str(resource_el.attrib["id"]) if resource_found else resource_id,
         node=node,
         timeout=timeout,
     )
diff --git a/pcs_test/tier0/lib/commands/resource/test_restart.py b/pcs_test/tier0/lib/commands/resource/test_restart.py
index 9d61f5704..af5004b43 100644
--- a/pcs_test/tier0/lib/commands/resource/test_restart.py
+++ b/pcs_test/tier0/lib/commands/resource/test_restart.py
@@ -104,20 +104,8 @@ class ResourceRestart(TestCase):
         )
 
     def test_resource_not_found(self):
-        self.env_assist.assert_raise_library_error(
-            lambda: resource.restart(self.env_assist.get_env(), "RX")
-        )
-        self.env_assist.assert_reports(
-            [
-                fixture.error(
-                    reports.codes.ID_NOT_FOUND,
-                    id="RX",
-                    expected_types=["resource"],
-                    context_type="",
-                    context_id="",
-                )
-            ]
-        )
+        self.config.runner.pcmk.resource_restart("RX")
+        resource.restart(self.env_assist.get_env(), "RX")
 
     def test_not_a_resource(self):
         self.env_assist.assert_raise_library_error(
-- 
2.48.1

